package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

	private final Course courseModel;
	private String[] columns = new String[]{"StudentID","Surname","Forename","Exam","Test","Assignment","Overall"};

	public CourseAdapter(Course courseModel) {
		this.courseModel = courseModel;
	}

	@Override
	public int getRowCount() {
		return courseModel.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		StudentResult studentResult = courseModel.getResultAt(rowIndex);

		switch (columnIndex){
			case 0:
				return studentResult._studentID;
			case 1:
				return studentResult._studentSurname;
			case 2:
				return studentResult._studentForename;
			case 3:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Exam);
			case 4:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Overall);
		}
		return null;
	}

	@Override
	public String getColumnName(int i) {
		return columns[i];
	}

	@Override
	public void courseHasChanged(Course course) {
		fireTableDataChanged();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}